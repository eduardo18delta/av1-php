<?php 

require_once 'classes/Conexao.php';

class Cadastrar {
	public $pdo;
	public $nome;
	public $rg;
	public $cpf;
	public $dtnasc;
	public $username;
	public $password;
	public $email;
	public $idcapturado;

	#Metódo para inserir na tabela Pessoa
	public function Inserirdados($nome,$rg,$cpf,$dtnasc){
        $conexao = Conexao::pegarConexao();
        $inserirdados = $conexao->prepare("INSERT INTO t_pessoa (p_id, p_nome, rg, cpf, dtnasc) VALUES (NULL, :NOME, :RG, :CPF, :DTNASC) ");
        $inserirdados->bindParam(":NOME", $nome, PDO::PARAM_STR);
        $inserirdados->bindParam(":RG", $rg, PDO::PARAM_STR);
        $inserirdados->bindParam(":CPF", $cpf, PDO::PARAM_STR);
        $inserirdados->bindParam(":DTNASC", $dtnasc, PDO::PARAM_STR);
        $inserirdados->execute();
	}

	#Metódo para buscar o p_id na tabela Pessoa
	public function BuscaID(){
		$conexao = Conexao::pegarConexao();
		$buscar_id = $conexao->prepare("SELECT max(p_id) as p_id FROM t_pessoa");	
		$buscar_id->execute();
		$idencontrado = $buscar_id->fetchAll(PDO::FETCH_OBJ);
		foreach ($idencontrado as $id) {
		 $this->idcapturado = $id->p_id;
		} 		 
	}

	#Metódo para inserir na tabela Login
	public function Inserirlogin($username,$password,$email){
		$this->BuscaID();
		$conexao = Conexao::pegarConexao();
        $inserirlogin = $conexao->prepare("INSERT INTO t_login (l_id, l_username, l_password, l_email, l_pessoafk) VALUES (NULL, :USERNAME, :PASSWORD, :EMAIL, :ID);");
        $inserirlogin->bindParam(":USERNAME", $username, PDO::PARAM_STR);
        $inserirlogin->bindParam(":PASSWORD", $password, PDO::PARAM_STR);
        $inserirlogin->bindParam(":EMAIL", $email, PDO::PARAM_STR);
        $inserirlogin->bindParam(":ID", $this->idcapturado, PDO::PARAM_STR);
        $inserirlogin->execute();
	}	
} 
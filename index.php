<?php 

$retorno  = $_REQUEST['reply'];

if ($retorno == "true") {
	$msg = "<script> alert('Cadastrado com sucesso!')</script>";
} else if ($retorno == "apagado") {
	$msg = "<script> alert('Apagado com sucesso!')</script>";
} 

?>

<!DOCTYPE html>
<html>
<head>
	<?php echo $msg; ?>
	<meta charset="utf-8">
	<meta name="Author" content="Eduardo Henrique">
	<meta name="Description" content="Atividade de PHP - AV1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/style.css"/>
	<title>Atividade POO - AV1</title>
</head>
<body>

<div class="container col-md-10">
<form action="inserir.php" method="post" class="mt-4">
	
<fieldset class="form-group">
<legend>Dados pessoais:</legend>	

<div class="form-group">
<label for="Nome">Nome:</label>	
<input class="form-control" type="text" name="nome">
</div>

<div class="form-group">
<label for="RG">RG:</label>	
<input class="form-control" type="text" name="rg">
</div>

<div class="form-group">
<label for="CPF">CPF:</label>	
<input class="form-control" type="text" name="cpf">
</div>

<div class="form-group">
<label for="Dtnasc">Data de Nascimento:</label>	
<input class="form-control" type="text" name="dtnasc">
</div>

</fieldset>

<fieldset class="form-group">
<legend>Dados de Login:</legend>	

<div class="form-group">
<label for="username">Username:</label>	
<input class="form-control" type="text" name="username">
</div>

<div class="form-group">
<label for="password">Senha:</label>	
<input class="form-control" type="text" name="password">
</div>

<div class="form-group">
<label for="email">Email:</label>	
<input class="form-control" type="text" name="email">
</div>

</fieldset>

<div class="form-group">
<button type="submit" class="btn btn-success">Cadastrar</button>
<button type="reset" class="btn btn-danger">Cancelar</button>
</div>

</form>

<?php
	require_once 'classes/Listar.php';
	$Listar = new Listar();
	$lista = $Listar->listar();

?>
<h1 class="text-center mb-5 mt-3 display-5s">Pessoas Cadastradas</h1>
<table class="table table-striped">

<tr>
	<th>ID</th>
	<th>Nome</th>
	<th>RG</th>
	<th>CPF</th>
	<th>Dt Nasc</th>
	<th>Ações</th>
</tr>

<?php foreach ($lista as $l): ?>

<tr>
	<td><?= $l['p_id']?></td>
	<td><?= $l['p_nome']?></td>
	<td><?= $l['rg']?></td>
	<td><?= $l['cpf']?></td>
	<td><?= $l['dtnasc']?></td>
	<td class="ml-2">
		<a title="Somente Exemplo" href="#" class="btn btn-info"><i class="far fa-edit"></i></a>
		<a title="Apagar" href="excluir.php?id=<?=$l['p_id']?>" class="btn btn-info" onclick="Pergunta()"><i class="fas fa-trash"></i></a></td>



	</td>
</tr>

<?php endforeach ?>
</table>

<script>

function Pergunta(){

decisao = confirm("Deseja realmente Apagar?");

}


</script>



</div>
</body>
</html>